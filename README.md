# jf/crypto

Cifrado y descifrado de textos y archivos.

## Instalación

### Composer

Este proyecto usa como gestor de dependencias [Composer](https://getcomposer.org) el cual puede ser instalado siguiendo
las instrucciones especificadas en la [documentación](https://getcomposer.org/doc/00-intro.md) oficial del proyecto.

Para instalar el paquete `jf/crypto` usando este manejador de paquetes se debe ejecutar:

```sh
composer require jf/crypto
```

### Control de versiones

Este proyecto puede ser instalado usando `git`. Primero se debe clonar el proyecto y luego instalar las dependencias:

```sh
git clone git@gitlab.com:joaquinfq/jfCrypto.git
cd jfCrypto
composer install
```

## Archivos disponibles

### Clases

| Nombre                                             | Descripción                                                                            |
|:---------------------------------------------------|:---------------------------------------------------------------------------------------|
| [jf\Crypto\ACipher](src/ACipher.php)               | Clase para cifrar de manera simple archivos usados como base de datos o configuración. |
| [jf\Crypto\Assert](src/Assert.php)                 | Excepción lanzada por el paquete.                                                      |
| [jf\Crypto\AsymmetricKeys](src/AsymmetricKeys.php) | Gestiona las necesidades el cifrado y descifrado usando claves asimétricas.            |
| [jf\Crypto\Cipher](src/Cipher.php)                 | Clase para cifrar de manera simple textos o datos.                                     |
| [jf\Crypto\FileCipher](src/FileCipher.php)         | Clase para cifrar de manera simple archivos usados como base de datos o configuración. |

### Interfaces

| Nombre                                               | Descripción                                                               |
|:-----------------------------------------------------|:--------------------------------------------------------------------------|
| [jf\Crypto\IAsymmetricKeys](src/IAsymmetricKeys.php) | Interfaz para las clases que gestionan claves criptográficas asimétricas. |
| [jf\Crypto\ICipher](src/ICipher.php)                 | Interfaz para cifrar/descifrar textos.                                    |
| [jf\Crypto\ICrypto](src/ICrypto.php)                 | Interfaz para las clases que gestionan claves criptográficas.             |
| [jf\Crypto\ICryptoId](src/ICryptoId.php)             | Interfaz para las clases que requieren cifrar su identificador.           |
| [jf\Crypto\IFileCipher](src/IFileCipher.php)         | Interfaz para cifrar/descifrar archivos.                                  |

### Traits

| Nombre                               | Descripción                                                                      |
|:-------------------------------------|:---------------------------------------------------------------------------------|
| [jf\Crypto\TId](src/TId.php)         | Trait para aplicar a los objetos que necesitan cifrar su identificador numérico. |

## Ejemplos

### jf\Crypto\AssymetricKeys

```php
// Si no tenemos las claves las creamos
// AsymmetricKeys::createKeys(__DIR__);

// Al crear la clase especificamos el directorio donde se encuentran las claves.
$ecdsa     = new AsymmetricKeys(__DIR__);
$content   = file_get_contents(__FILE__);
$encrypted = $ecdsa->encrypt($content);
assert($ecdsa->decrypt($encrypted) === $content);
```

### jf\Crypto\FileCipher

```php
$content    = file_get_contents(__FILE__);
$key        = md5($content); // Asignamos la clave que queremos usar.
$cipher     = new Cipher($key);
$filecipher = new FileCipher($key);
$encrypted  = $cipher->encrypt($content);
$encfile    = __FILE__ . '.encrypted';

assert($cipher->decrypt($encrypted) === $content);
assert($filecipher->save($encfile, $content) === strlen($encrypted));
assert($filecipher->load($encfile) === $content);

unlink($encfile);
```