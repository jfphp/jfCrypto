<?php

namespace jf\Crypto;

use Exception as PhpException;

/**
 * Excepción lanzada por el paquete.
 * Los mensajes de error no dan pistas de los nombres de archivos o directorios.
 */
class Assert extends PhpException
{
    /**
     * Código de error cuando el método de cifrado de no es válido.
     */
    public const ERROR_ALGORITHM_UNKNOWN = 1_500;

    /**
     * Código de error cuando el directorio no existe.
     */
    public const ERROR_NOT_IS_DIR = 1_404;

    /**
     * Código de error cuando el archivo no existe.
     */
    public const ERROR_NOT_IS_FILE = 2_404;

    /**
     * Código de error cuando el archivo no se puede leer.
     */
    public const ERROR_NOT_IS_READABLE = 2_500;

    /**
     * Código de error cuando el directorio no se puede crear.
     */
    public const ERROR_MKDIR_FAILED = 3_500;

    /**
     * Verifica si el directorio existe.
     *
     * @param string $directory Ruta del directorio a verificar.
     *
     * @return void
     */
    public static function isDir(string $directory) : void
    {
        if (!is_dir($directory))
        {
            throw new static(dgettext('crypto', 'Directorio no encontrado'), self::ERROR_NOT_IS_DIR);
        }
    }

    /**
     * Verifica si el archivo existe.
     *
     * @param string $file Ruta del archivo a verificar.
     *
     * @return void
     */
    public static function isFile(string $file) : void
    {
        if (!is_readable($file))
        {
            throw new static(dgettext('crypto', 'Archivo no encontrado'), self::ERROR_NOT_IS_FILE);
        }
    }

    /**
     * Verifica si el archivo se puede leer.
     *
     * @param string $file Ruta del archivo a verificar.
     *
     * @return void
     */
    public static function isReadable(string $file) : void
    {
        if (!is_readable($file))
        {
            throw new static(dgettext('crypto', 'El archivo no se puede leer'), self::ERROR_NOT_IS_READABLE);
        }
    }

    /**
     * Verifica si el método de cifrado es válido.
     *
     * @param string $algorithm Nombre del método o algoritmo a verificar.
     *
     * @return void
     */
    public static function methodValid(string $algorithm) : void
    {
        if (!in_array($algorithm, openssl_get_cipher_methods()))
        {
            throw new static(dgettext('crypto', 'Método de cifrado desconocido'), self::ERROR_ALGORITHM_UNKNOWN);
        }
    }

    /**
     * Verifica que el directorio exista o pueda ser creado.
     *
     * @param string $directory Ruta del directorio a verificar.
     *
     * @return void
     */
    public static function mkdir(string $directory) : void
    {
        if (!is_dir($directory) && !mkdir($directory, 0o777, TRUE))
        {
            throw new static(dgettext('crypto', 'El directorio no pudo ser creado'), self::ERROR_MKDIR_FAILED);
        }
    }
}