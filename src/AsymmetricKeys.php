<?php

namespace jf\Crypto;

/**
 * Gestiona las necesidades el cifrado y descifrado usando claves asimétricas.
 */
class AsymmetricKeys implements IAsymmetricKeys
{
    /**
     * Nombre del archivo con la clave privada.
     *
     * @var string
     */
    public const PRV_KEY = 'prv_key.pem';

    /**
     * Nombre del archivo con la clave pública.
     *
     * @var string
     */
    public const PUB_KEY = 'pub_key.pem';

    /**
     * Ruta del directorio con las claves asimétricas.
     *
     * @var string
     */
    private string $_directory = '';

    /**
     * Clave privada.
     *
     * @var string
     */
    private string $_privateKey = '';

    /**
     * Clave pública.
     *
     * @var string
     */
    private string $_publicKey = '';

    /**
     * Constructor de la clase.
     *
     * @param string $directory Ruta del directorio con las claves asimétricas.
     */
    public function __construct(string $directory = '')
    {
        Assert::isDir($directory);
        $this->_directory = $directory;
    }

    /**
     * @inheritdoc
     */
    public static function createKeys(string $outdir, int $type = OPENSSL_KEYTYPE_RSA, ?int $bits = 4096) : void
    {
        $config = [ 'private_key_type' => $type ];
        if ($bits !== NULL)
        {
            $config['private_key_bits'] = $bits;
        }
        $openssl = openssl_pkey_new($config);
        Assert::mkdir($outdir);
        openssl_pkey_export_to_file($openssl, $outdir . '/' . static::PRV_KEY);
        file_put_contents($outdir . '/' . static::PUB_KEY, openssl_pkey_get_details($openssl)['key']);
    }

    /**
     * @inheritdoc
     */
    public function decrypt(string $encrypted) : ?string
    {
        $message = NULL;
        openssl_private_decrypt(hex2bin($encrypted), $message, $this->privateKey());

        return $message;
    }

    /**
     * @inheritdoc
     */
    public function encrypt(string $content) : string
    {
        if (openssl_public_encrypt($content, $encrypted, $this->publicKey()) && $encrypted)
        {
            $encrypted = bin2hex($encrypted);
        }

        return $encrypted ?: '';
    }

    /**
     * @inheritdoc
     */
    public function privateKey() : string
    {
        return $this->_privateKey ?: ($this->_privateKey = $this->_readFile(static::PRV_KEY));
    }

    /**
     * @inheritdoc
     */
    public function publicKey() : string
    {
        return $this->_publicKey ?: ($this->_publicKey = $this->_readFile(static::PUB_KEY));
    }

    /**
     * Lee un archivo con la clave y devuelve su contenido.
     *
     * @param string $file Nombre del archivo a leer.
     *
     * @return string
     */
    private function _readFile(string $file) : string
    {
        $filename = $this->_directory . '/' . $file;
        Assert::isFile($filename);
        Assert::isReadable($filename);

        return file_get_contents($filename) ?: '';
    }
}