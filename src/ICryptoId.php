<?php

namespace jf\Crypto;

use jf\Base\IId;

/**
 * Interfaz para las clases que requieren cifrar su identificador.
 */
interface ICryptoId extends IId
{
    /**
     * Devuelve la clase usada para cifrar y descrifrar el identificador,
     *
     * @return ICrypto
     */
    public function crypto() : ICrypto;
}