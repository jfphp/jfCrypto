<?php

namespace jf\Crypto;

/**
 * Clase para cifrar de manera simple archivos usados como base de datos o configuración.
 * No pretende evitar al 100% que se descifre su contenido sino evitar que se
 * encuentren coincidencias usando herramientas como `grep`, `strings`, etc.
 */
class FileCipher extends Cipher implements IFileCipher
{
    /**
     * @inheritdoc
     */
    public function load(string $filename) : string
    {
        Assert::isReadable($filename);
        $data = file_get_contents($filename);

        return $data
            ? $this->decrypt($data) ?? ''
            : '';
    }

    /**
     * @inheritdoc
     */
    public function save(string $outfile, string $data) : ?int
    {
        $outdir = dirname($outfile);
        Assert::mkdir($outdir);
        $result = file_put_contents($outfile, $this->encrypt($data));

        return $result === FALSE ? NULL : $result;
    }
}
