<?php

namespace jf\Crypto;

/**
 * Clase para cifrar de manera simple archivos usados como base de datos o configuración.
 * No pretende evitar al 100% que se descifre su contenido sino evitar que se
 * encuentren coincidencias usando herramientas como `grep`, `strings`, etc.
 */
abstract class ACipher implements ICipher
{
    /**
     * @inheritdoc
     */
    public function decrypt(string $encrypted) : ?string
    {
        $algorithm = $this->algorithm();
        if ($algorithm)
        {
            $ivlen     = openssl_cipher_iv_length($algorithm);
            $encrypted = openssl_decrypt(substr($encrypted, $ivlen), $algorithm, $this->key(), 0, substr($encrypted, 0, $ivlen));
            if ($encrypted === FALSE)
            {
                $encrypted = NULL;
            }
        }

        return $encrypted;
    }

    /**
     * @inheritdoc
     */
    public function encrypt(string $content) : string
    {
        $algorithm = $this->algorithm();
        if ($algorithm)
        {
            $ivlen = openssl_cipher_iv_length($algorithm);
            $iv    = $ivlen
                ? openssl_random_pseudo_bytes($ivlen)
                : '';
            // El IV no debería almacenarse en el resultado pero hacemos una excepción por no requerir
            // ninguna seguridad más allá de no ver el contenido del archivo a simple vista.
            $content = $iv . openssl_encrypt($content, $algorithm, $this->key(), 0, $iv);
        }

        return $content;
    }
}
