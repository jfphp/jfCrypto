<?php

namespace jf\Crypto;

/**
 * Clase para cifrar de manera simple textos o datos.
 */
class Cipher extends ACipher
{
    /**
     * Algoritmo a usar.
     *
     * @var string
     */
    private readonly string $_algorithm;

    /**
     * Clave a usar para cifrar/descifrar.
     *
     * @var string
     */
    private readonly string $_key;

    /**
     * Cipher constructor.
     *
     * @param string $key       Clave a usar para cifrar/descifrar.
     * @param string $algorithm Algoritmo a usar.
     */
    public function __construct(string $key = '', string $algorithm = 'aes-128-cbc')
    {
        $this->_algorithm = $algorithm ?: 'aes-128-cbc';
        Assert::methodValid($this->_algorithm);
        // Por simplicidad y para no almacenarla en un repositorio si no se ha inicializado la clave a
        // usar se obtiene del nombre de la clase.
        // Hay que tener presente que si la clase la hereda otra, la clase hija no podrá descifrar el archivo.
        // Para información la clave debería ser recibida en el parámetro `$key`.
        $this->_key = $key ?: md5(static::class);
    }

    /**
     * @inheritdoc
     */
    public function algorithm() : string
    {
        return $this->_algorithm;
    }

    /**
     * @inheritdoc
     */
    public function key() : string
    {
        return $this->_key;
    }
}
