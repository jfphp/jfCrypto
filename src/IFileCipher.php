<?php

namespace jf\Crypto;

/**
 * Interfaz para cifrar/descifrar archivos.
 */
interface IFileCipher extends ICipher
{
    /**
     * Lee el contenido del archivo y lo devuelve descifrado.
     *
     * @param string $filename Ruta completa del archivo a leer.
     *
     * @return string
     */
    public function load(string $filename) : string;

    /**
     * Guarda cifrados en disco los datos especificados.
     *
     * @param string $outfile Ruta completa del archivo a escribir.
     * @param string $data    Datos a escribir el archivo.
     *
     * @return int|null Cantidad de bytes escritos o `FALSE` si hubo un error.
     */
    public function save(string $outfile, string $data) : ?int;
}
