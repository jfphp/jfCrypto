<?php

namespace jf\Crypto;

/**
 * Interfaz para las clases que gestionan claves criptográficas asimétricas.
 */
interface IAsymmetricKeys extends ICrypto
{
    /**
     * Crea las claves criptográficas para poder cifrar y descifrar los textos.
     *
     * @param string   $outdir Directorio donde colocar las claves.
     * @param int      $type   Tipo de claves a generar.
     * @param int|null $bits   Cantidad de bits a usar para las claves.
     *
     * @return void
     */
    public static function createKeys(string $outdir, int $type = OPENSSL_KEYTYPE_RSA, ?int $bits = 4096) : void;

    /**
     * Devuelve el valor de la clave privada.
     *
     * @return string
     */
    public function privateKey() : string;

    /**
     * Devuelve el valor de la clave pública.
     *
     * @return string
     */
    public function publicKey() : string;
}