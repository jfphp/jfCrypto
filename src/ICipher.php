<?php

namespace jf\Crypto;

/**
 * Interfaz para cifrar/descifrar textos.
 */
interface ICipher extends ICrypto
{
    /**
     * Devuelve el algoritmo a usar para cifrar/descifrar.
     *
     * @return string
     */
    public function algorithm() : string;

    /**
     * Construye la clave de cifrado.
     *
     * @return string
     */
    public function key() : string;
}
