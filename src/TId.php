<?php

namespace jf\Crypto;

use jf\Base\IId as IBaseId;
use Throwable;

/**
 * Trait para aplicar a los objetos que necesitan cifrar su identificador numérico.
 *
 * @mixin IBaseId
 * @mixin ICryptoId
 */
trait TId
{
    /**
     * Identificador del elemento.
     *
     * @var int|string|null
     */
    public int|string|null $id = NULL;

    /**
     * @see ICryptoId::crypto()
     */
    abstract public function crypto() : ICrypto;

    /**
     * @see ICryptoId::getId()
     */
    public function getId() : int|string|null
    {
        $id = $this->id;
        if ($id === '')
        {
            $id = NULL;
        }
        elseif ($id !== NULL)
        {
            try
            {
                $id = $this->crypto()->encrypt((string) $id);
            }
            catch (Throwable)
            {
            }
        }

        return $id;
    }

    /**
     * @see ICryptoId::setId()
     */
    public function setId(int|string|null $id) : static
    {
        if (is_string($id) && $id !== '')
        {
            try
            {
                $cryptoId = $this->crypto()->decrypt($id);
                if ($cryptoId)
                {
                    $id = $cryptoId;
                }
            }
            catch (Throwable)
            {
            }
        }
        $this->id = $id;

        return $this;
    }
}