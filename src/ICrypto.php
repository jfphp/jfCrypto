<?php

namespace jf\Crypto;

/**
 * Interfaz para las clases que cifra y descifran contenido.
 */
interface ICrypto
{
    /**
     * Descifra el texto.
     *
     * @param string $encrypted Texto a descifrar.
     *
     * @return string|NULL
     */
    public function decrypt(string $encrypted) : ?string;

    /**
     * Cifra el contenido.
     *
     * @param string $content Contenido a cifrar.
     *
     * @return string
     */
    public function encrypt(string $content) : string;
}