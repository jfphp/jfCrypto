<?php

use jf\Crypto\Cipher;
use jf\Crypto\FileCipher;

require_once __DIR__ . '/../../autoload.php';

/**
 * Compara la longitud resultante al cifrar el texto especificado
 * con todos los métodos de cifrado permitidos.
 *
 * @param string $text Texto a cifrar.
 *
 * @return void
 */
function cmpMethods(string $text = 'abcdefghijklmnopqrstuvwxyz') : void
{
    $algos = [];
    $key   = md5($text);
    foreach (openssl_get_cipher_methods() as $algorithm)
    {
        echo $algorithm . PHP_EOL;
        $cipher = new Cipher($key, $algorithm);
        $e      = @$cipher->encrypt($text);
        if (strlen($e) > 0)
        {
            $algos[ $algorithm ] = strlen(bin2hex($e));
        }
    }
    arsort($algos);
    die(yaml_emit($algos));
}

$algorithm  = 'sm4-ecb';
$content    = file_get_contents(__FILE__);
$encfile    = __FILE__ . '.encrypted';
$key        = md5($content);
$cipher     = new Cipher($key, $algorithm);
$encrypted  = $cipher->encrypt($content);
$cipherfile = new FileCipher($key, $algorithm);

assert($cipher->decrypt($encrypted) === $content);
assert($cipherfile->save($encfile, $content) === strlen($encrypted));
assert($cipherfile->load($encfile) === $content);
// Los contenidos del archivo cifrado y $encrypted no se pueden comparar ya que al tener diferentes IV no coinciden.
// assert(file_get_contents($encfile) === $encrypted);
unlink($encfile);

cmpMethods();