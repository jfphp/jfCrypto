<?php

use jf\Crypto\AsymmetricKeys;

require_once __DIR__ . '/../../autoload.php';

//AsymmetricKeys::createKeys(__DIR__);
$ecdsa     = new AsymmetricKeys(__DIR__);
$content   = file_get_contents(__FILE__);
$encrypted = $ecdsa->encrypt($content);
assert($ecdsa->decrypt($encrypted) === $content);
